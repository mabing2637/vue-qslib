import BaseOption from './BaseOption';
import DataDefine, { QuestionDataTypes } from './QuestionDataTypes';
export default class FillBlankOption extends BaseOption {
    public DataType: QuestionDataTypes = QuestionDataTypes.dtText;
    public InputLength: number = 50;

}
