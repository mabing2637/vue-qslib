
import {Component, Vue, Watch, Prop } from 'vue-property-decorator'
import BaseQuestionSubject from './TQuestionSubject';
import BaseOption from './BaseOption';
import DataDefine, { QuestionDataTypes } from './QuestionDataTypes';
import OrderBy, {OrderByTypes} from './OrderByTypes';
import GUID from './GUID';
import MatrixRadioSubject from './MultiSelSubject';
/**
 * 题目选项的基本操作定义
 */

export default abstract class TBaseQuestion<T extends BaseOption, S extends BaseQuestionSubject<T>> extends Vue {

    public Data!: S;

    public canEdit: boolean = true;

    public OrderByItems: OrderBy[] = [];

    public visible=false;
    public see="显示";
    public show=true; 
   //离线（默认false）
    public lixian = false;
    //预览
    public preview=false;

    public tabledata:string[]=[];
    public columitem:Anwer[]=[];
    
    constructor(private obj: new() => T, private sObj: new() => S) {
        super();
        this.Data = this.getSInstance();
       
        this.Data.DataType = QuestionDataTypes.dtText;

    }

   
    // 创建泛型实列
    public getInstance(): T {
        return new this.obj();
    }

    public getSInstance(): S {
        return new this.sObj();
    }

    public doSomething(): void {
        const s = this.getSInstance();
        const b = this.getInstance();

        s.Options.push(this.getInstance());


    }
    
    // 获取题目数据
    public abstract getQuestionData(): string;
    // 设置题目数据
    public abstract setQuestionData(data: string): void;

    public abstract insert(id: string): void;

    created(){
        if(this.canedit)
        {
            this.canEdit=this.canedit;
        }
    }

    // 判断输入内容类型
    public JudgeInputType(input: string): void {
        const num: number = parseFloat(input);
        if (num == undefined) {
            input = '0';
        }

    }


    public GetDataType(typeValue: QuestionDataTypes): void {
        this.Data.DataType = typeValue;
    }

    // 添加新的选项
    public createNewOption(c: new() => T): T {
        const options = this.Data.Options;
        const opt = new c();
        opt.itemId = new GUID().toString();
        opt.itemName = '选项' + options.length;

        return opt;
    }

    public createTObject<TT>(c: new() => TT): TT {
        const obj = new c();
        return obj;
    }

    // 移除选项
    public remove(id: string): void {
        const options = this.Data.Options;
        options.splice(options.findIndex((item) => item.itemId === id), 1);
    }
    // 选项上移动
    public moveUp(id: string): void {
        const options = this.Data.Options;
        const index = options.findIndex((item) => item.itemId === id);

           if (index === 0) {
        return;
           }

           const upObj = options[index - 1];
           const moveObj = options[index];
           options.splice(index, 1, upObj); // (索引，长度，值)
           options.splice(index - 1, 1, moveObj); // (索引，长度，值)
    }

    // 选项向下移动
    public moveDown(id: string): void {
        const options = this.Data.Options;
        const index = options.findIndex((item) => item.itemId === id);



           if (index === (options.length - 1)) {
        return;
           }

           const downObj = options[index + 1];
           const moveObj = options[index];
           options.splice(index, 1, downObj); // (索引，长度，值)
           options.splice(index + 1, 1, moveObj); // (索引，长度，值)
    }
    public hasGotType(type: string): void {

    }

    public setEdit(): void {
        this.canEdit = !this.canEdit;

    }
    public SetTitle(_title: string): void {
        this.Data.Title = _title;
    }
    /*
    setCodeNum(code:string):void
    {
        this.Data.CodeNum=code;
    }
    */
    public setQuestionId(qId: string): void {
        this.Data.Id = qId;
    }

    public setSelValue(value: string): void {
        this.Data.SelValue = value;
    }

    public setDataType(dataType: QuestionDataTypes) {
       this.Data.DataType = dataType;
    }

    public GetHtml(html: string): void {
        this.Data.Title = html;
    }
    //是否可见
    public divvisibility(id:number){
        switch (id) {
            case 1:
                this.visible=!this.visible;
                this.see=this.visible?"隐藏":"显示";
                break;
        
            case 2:
                this.setdata();
                this.visible=!this.visible;
                this.see=this.visible?"隐藏":"显示";
                break;
        }
            
      }
    // 行列转置方法
    setdata() {
        this.tabledata=[];
        this.columitem=[];
        if(this.show===true){//选项横置
           for(let i=0;i<this.Data.RowTitles.length;i++){
               this.tabledata[i]=this.Data.RowTitles[i].Title;
           }
           for(let i=0;i<this.Data.Options.length;i++){
               let an=new Anwer();
               an.optionId=this.Data.Options[i].itemName;
               an.checkvalue=new Array<string>(this.Data.RowTitles.length);
               an.blankvalue=new Array<string>(this.Data.RowTitles.length);
               this.columitem.push(an);
           }
        }else if(this.show===false){//选项纵向
           for(let i=0;i<this.Data.Options.length;i++){
               this.tabledata[i]=this.Data.Options[i].itemName;
           }
           for(let i=0;i<this.Data.RowTitles.length;i++){
               let an=new Anwer();
               an.optionId=this.Data.RowTitles[i].Title;
               an.checkvalue=new Array<string>(this.Data.Options.length);
               an.blankvalue=new Array<string>(this.Data.Options.length);
               this.columitem.push(an);
           }
           
        }
    }
//初始化数据
    public initProps():void
    {
          //初始化数据
          if(this.initdata)
          {
              this.Data=this.initdata;
 
              console.info("initdata title:"+this.initdata.Title);
              console.info("this data title:"+this.Data.Title);
          }
           if(this.canedit)
         {
             this.canEdit=this.canedit;
         }

         if(this.codenum)
         {
             this.Data.CodeNum=this.codenum;
             console.info("codenum:"+this.codenum);
         }
    }

    mounted()
     {
         console.info("TBaseQuestion mounted");
         this.$emit('mounted',this);
     }
    
    @Prop() initdata!: S;
    @Prop() canedit!:boolean;
    @Prop() codenum!:string;

    @Watch('Data')
    watchComData(newVal:S, oldVal:S) {
     
         console.log("qs data Changed:"+newVal);
         if(newVal==null)
         return;
         this.$emit("input",newVal);
    }
    @Watch('preview')
    ifpreview(){
        if(this.preview){
            this.visible=true;
        }else{
            this.visible=true;
        }
    }
  
}
export class Anwer{
    
     public optionId="";
     public checkvalue:string[]=[];
     public blankvalue:string[]=[];
     constructor(){
         
 }
 }
 

